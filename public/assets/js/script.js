// alert("hello")
// 
// Array literal = square brackets
// let dabarkads = ["tito", "vic", "joey"];
// console.log(dabarkads[0]);
// console.log(dabarkads[2]);
// console.log(dabarkads.length);

// // bale ung length sa array, kung ilan ung laman niya, 
// // then ung index is from 0
// let grades = [98,85,100,90];
// for(let i = 0; i < grades.length;i++){
// 	console.log(grades[i]);
// }

// // Reassignment
// let dabarkadsOne = ["tito", "vic", "joey"];
// dabarkadsOne[1] = "coney";
// dabarkadsOne[2] = "aiza"

// console.log(dabarkadsOne);


// //Sort method - inaarrange niya alpahabetically

// let sortedDabarkadsOne = dabarkadsOne.sort();
// console.log(sortedDabarkadsOne);

// //push - mag a-add ng new items sa end ng array

// let dabarkadsThree = ["Tito", "Vic", "Joey"];
// dabarkadsThree.push("Aiza");
// dabarkadsThree.push("Jimmy");
// dabarkadsThree.push("Ruby");
// console.log(dabarkadsThree);


// //pop - opposite siya ni push, nag reremove siya ng last item sa array

// // let dabarkadsFour = ["Tito", "Vic", "Joey"];
// // dabarkadsFour.pop();

// // console.log(dabarkadsFour);

// //reverse - nirereverse niya ung order ng elements
// let dabarkadsFour = ["Tito", "Vic", "Joey"];
// dabarkadsFour.reverse();
// console.log(dabarkadsFour);

// //shift - removes the first element in the array and value of the removed element can be registered to a variable

// let dabarkadsFive = ["Tito", "Vic", "Joey"];
// let friends = dabarkadsFive.shift();
// console.log(friends);
// console.log(dabarkadsFive)

// //unshift - add one or more elements at the beginning of the array


// let newFriends = dabarkads.unshift("Areej", "Gabrielle");
// console.log(newFriends);

// //concat - concatenating two existing arrays
// //
// let dabarkadsSix = ["Tito", "Vic", "Joey"];
// let friendsSix = ["Coney", "Aiza", "Jimmy"];

// let eatBulaga = dabarkadsSix.concat(friendsSix);
// console.log(eatBulaga);

// //splice - cam remove a specified number elements starting on a given index..
// //
// let favFoods = ["burger","pizza","bread","pasta","donut"];

// favFoods.splice(3); //tinaggal niya ung una at pangalawang item
// console.log(favFoods);

// //slice - can return a brand new array

// let colors = ["red","orange", "yellow", "green", "blue", "indigo", "violet"];


// console.log(colors);
// //sa first number, index the ung second number yung kung pang ilan talaga
// let slice = colors.slice(2,5);

// console.log	(slice);


//split is used when you want to take a string and chop it into an array
// let greet = "h,e,l,l,o";
// let greetArray = greet.split(",");

// let todo = "laundry, eating, reading, sleeping";
// let todoList = todo.split(",");

// console.log(greetArray);
// console.log(todoList);

// //join - opposite ni split  si join

// let todoListOne = ["laundry, eating, reading, sleeping"];
// let todoOne = todoListOne.join(",");
// console.log(todoOne);
// //toString() function converts an array into a single value that is separated by comma

// let greetArrayOne = ["h","e","l","l","o"];
// console.log(greetArray.toString());


// //indexOf - finds the index of the given element where it it first found
// //
// let todoListThree = ["laundry, eating, reading, sleeping"];
// let indexTwo = todoListThree.indexOf("reading");
// console.log(indexTwo);

//foreach() - similar to a for loop that iterates on each array element 
// let todoList = ["laundry, eating, reading, sleeping","reading"];

// function logtodoList(element,index){
// 	console.log(index,element);

// }
// todoList.forEach(logtodoList);

// todoList.forEach(function(logtodoList){
// 	console.log(logtodoList);
// });

//map function creates a new array from an existing after calling a function on every element

// let numbersMap = numbers.map(function(number){
// 	return number * number;
// })
// console.log(numbersMap);

// let names = ["Christian", "Ken", "Jacob"]

// let greetings = names.map(function(name){
// 	return "My name is " + name;
// })
// console.log(greetings);

//every checks if all the elements passes in a given condition
//some() checks if at least one elements passes a given condition.
// let allValid = numbers.every(function(number){
// 	return (number < 3);
// });
// console.log(allValid);

// let someValid = numbers.some(function(number){
// 	return (numbers < 3);
// });
// console.log(someValid);

//filter
//
// let numbers = [1,2,3,4,5];
// let filterNum = numbers.filter(function(number){
// 	return(number < 3);
// });
// console.log(filterNum);


// //reduce
// let reduceNum = numbers.reduce(function(x,y){
// 	return x + y;
// });
// console.log(reduceNum);


/******************************/

// DOM Document Object MOdel 
// 
let submit = document.getElementById('submit');
let todo = [];


submit.addEventListener('click', function() {
    let task = document.getElementById('tasks').value;
    todo.push(task);
    console.log(todo);

    let list = "";

    for(let i = 0; i < todo.length; i++){
    	list += todo[i];
    }
});